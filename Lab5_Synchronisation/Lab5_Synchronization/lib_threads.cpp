#include "header_main.h"

DWORD WINAPI ThreadCalcIntegral(LPVOID lParam)
{
    Buffer *data = (Buffer *)lParam;

    // ����������� �������:
    for(int i = 0; i < NUM_EVENTS; i++)
        data->hEvents[i] = CreateEvent(NULL,TRUE, FALSE, NULL);

    // �������� �������:
    data->hThreads[ID_THREAD_ALPHA] = CreateThread(NULL,0,ThreadCoefAlpha,data,0,NULL);
    data->hThreads[ID_THREAD_BETA] = CreateThread(NULL,0,ThreadCoefBeta,data,0,NULL);
    data->hThreads[ID_THREAD_Y] = CreateThread(NULL,0,ThreadYValue,data,0,NULL);
    data->hThreads[ID_THREAD_GAMMA] = CreateThread(NULL,0,ThreadCoefGamma,data,0,NULL);
    data->hThreads[ID_THREAD_INTEGRAND] = CreateThread(NULL,0,ThreadIntegrand,data,0,NULL);
    data->hThreads[ID_THREAD_FILE]= CreateThread(NULL,0,ThreadOutputToFile,data,0,NULL);

    while (true)
    {
        WaitForSingleObject(data->hEventMain, INFINITE);

        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) // ���������� �����������
        {
            //��������� ��� ������ ��� ������:
            for(int i = 0; i < NUM_EVENTS; i++)
                SetEvent(data->hEvents[i]);
            break;
        }

        data->time = clock(); // ������ ����� ����������

        data->result = 0;
        for (int i = 0; i < data->numOfParts; i++)
        {
            data->xCurrent = data->xBegin + i * data->deltaX;

            SetEvent(data->hEvents[ID_EVENT_ALPHA]);
            WaitForSingleObject(data->hEvents[ID_EVENT_INTEGRAL], INFINITE);

            data->result += data->func * data->deltaX;

            ResetEvent(data->hEvents[ID_EVENT_INTEGRAL]);
        }

        data->time = clock() - data->time;          // ������������� ����� ����������
        data->time = data->time / CLOCKS_PER_SEC;

        sprintf(data->messageLog, "           �������� %s\n����� �������:    %f\n������ �������:   %f\n����� ���������:  %d\n���������:        %f\n����� ����������: %f",
                data->intervalName, data->xBegin, data->xEnd, data->numOfParts, data->result, data->time);
        sprintf(data->message, "              �������� %s\n����� �������:          %f\n������ �������:       %f\n����� ���������:    %d\n���������:                   %f\n����� ����������: %f",
                data->intervalName, data->xBegin, data->xEnd, data->numOfParts, data->result, data->time);

        SetEvent(data->hEvents[ID_EVENT_FILE]); // ���� ������ �� ������ � ����
        WaitForSingleObject(data->hEvents[ID_EVENT_INTEGRAL], INFINITE); // �����, ������ ���������
        ResetEvent(data->hEvents[ID_EVENT_INTEGRAL]);

        SetEvent(data->hEventReady); // ���� ������ �� ����� ���������
        WaitForSingleObject(data->hEvents[ID_EVENT_INTEGRAL], INFINITE); // �����, ������ ���������
        ResetEvent(data->hEvents[ID_EVENT_INTEGRAL]);

        ResetEvent(data->hEventMain);
    }

    WaitForMultipleObjects(NUM_THREADS,data->hThreads,TRUE,INFINITE);

    for(int i = 0; i < NUM_THREADS; i++)
        CloseHandle(data->hThreads[i]);

    for(int i = 0; i < NUM_EVENTS; i++)
        CloseHandle(data->hEvents[i]);

    return 0;
}

DWORD WINAPI ThreadCoefAlpha(LPVOID lParam)
{
    Buffer *data = (Buffer *)lParam;

    while(true)
    {
        WaitForSingleObject(data->hEvents[ID_EVENT_ALPHA], INFINITE);
        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) break; //��� ����������� ����������

        for(int i = 0; i < MAX_BUF_COEF_SIZE; i++)
        {
            data->alpha[i] = sin(data->xCurrent) * i;
        }

        ResetEvent(data->hEvents[ID_EVENT_ALPHA]);
        SetEvent(data->hEvents[ID_EVENT_BETA]);
    }

    return 0;
}

DWORD WINAPI ThreadCoefBeta(LPVOID lParam)
{
    Buffer *data = (Buffer *)lParam;

    while(true)
    {
        WaitForSingleObject(data->hEvents[ID_EVENT_BETA], INFINITE);
        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) break; //��� ����������� ����������

        for (int i = 0; i < MAX_BUF_COEF_SIZE; i++)
        {
            data->beta[i] = data->alpha[i] * cos(data->xCurrent) * i;
        }

        ResetEvent(data->hEvents[ID_EVENT_BETA]);
        SetEvent(data->hEvents[ID_EVENT_Y]);
    }

    return 0;
}

DWORD WINAPI ThreadYValue(LPVOID lParam)
{
    Buffer *data = (Buffer *)lParam;

    while(true)
    {
        WaitForSingleObject(data->hEvents[ID_EVENT_Y], INFINITE);
        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) break; //��� ����������� ����������

        for(int i = 0; i < MAX_BUF_COEF_SIZE; i++)
        {
            data->yValue[i] = 0;
            for(int j = 1; j < i + 1; j++)
                data->yValue[i] += exp(1.0 / data->alpha[j]) + exp(1.0 / data->beta[j]);
        }

        ResetEvent(data->hEvents[ID_EVENT_Y]);
        SetEvent(data->hEvents[ID_EVENT_GAMMA]);
    }

    return 0;
}


DWORD WINAPI ThreadCoefGamma(LPVOID lParam)
{
    Buffer *data = (Buffer *)lParam;

    while(true)
    {
        WaitForSingleObject(data->hEvents[ID_EVENT_GAMMA], INFINITE);
        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) break; //��� ����������� ����������

        for(int i = 0; i < MAX_BUF_COEF_SIZE; i++)
        {
            data->gamma[i] = sin(data->xCurrent) * cos(data->yValue[i]) * i;
        }

        ResetEvent(data->hEvents[ID_EVENT_GAMMA]);
        SetEvent(data->hEvents[ID_EVENT_INTEGRAND]);
    }

    return 0;
}

DWORD WINAPI ThreadIntegrand(LPVOID lParam)
{
    Buffer *data = (Buffer *)lParam;

    while(true)
    {
        WaitForSingleObject(data->hEvents[ID_EVENT_INTEGRAND], INFINITE);
        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) break; //��� ����������� ����������

        data->func = 0;
        for (int i = 0; i < 1001; i++)
        {
            data->func += data->alpha[i] + data->beta[i] + data->gamma[i];
        }
        data->func *= log(1.0 + data->xCurrent);

        ResetEvent(data->hEvents[ID_EVENT_INTEGRAND]);
        SetEvent(data->hEvents[ID_EVENT_INTEGRAL]);
    }

    return 0;
}

DWORD WINAPI ThreadOutputToFile(LPVOID lParam)
{
    Buffer *data = (Buffer *)lParam;

    while(true)
    {
        WaitForSingleObject(data->hEvents[ID_EVENT_FILE], INFINITE);
        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) break; //��� ����������� ����������

        WaitForSingleObject(hSemaphoreFile, INFINITE);

        FILE* file = fopen("log.txt","a");
        fprintf(file, "\n\n");

        // ������� ����:
        time_t seconds = time(NULL);
        tm* timeinfo = localtime(&seconds);
        fprintf(file, asctime(timeinfo));

        //������� ���������� � �����������:
        fprintf(file, data->messageLog);

        fclose(file);

        ReleaseSemaphore(hSemaphoreFile, 1, NULL);
        ResetEvent(data->hEvents[ID_EVENT_FILE]);
        SetEvent(data->hEvents[ID_EVENT_INTEGRAL]);
    }

    return 0;
}

DWORD WINAPI ThreadOutputCommonInfo(LPVOID lParam)
{
    while(true)
    {
        WaitForSingleObject(bufAB->hEventReady, INFINITE);
        WaitForSingleObject(bufCD->hEventReady, INFINITE);
        if(WaitForSingleObject(hEventExit, 1) == WAIT_OBJECT_0) break; //��� ����������� ����������

        char message[MAX_SIZE_MESSAGE];
        strcpy(message, bufAB->message);
        strcat(message, "\n---------------------------\n");
        strcat(message, bufCD->message);
        strcat(message, "\n---------------------------\n");
        char temp[100];
        sprintf(temp, "����� ����� ����������: %f", bufAB->time + bufCD->time);
        strcat(message, temp);

        FILE* file = fopen("log.txt","a");
        fprintf(file, "\n\n");
        fprintf(file, temp);
        fclose(file);

        MessageBox(*hwndMain, message,"���������", MB_OK|MB_ICONINFORMATION);

        ResetEvent(bufAB->hEventReady);
        ResetEvent(bufCD->hEventReady);
        SetEvent(bufAB->hEvents[ID_EVENT_INTEGRAL]);
        SetEvent(bufCD->hEvents[ID_EVENT_INTEGRAL]);
    }

    return 0;
}
