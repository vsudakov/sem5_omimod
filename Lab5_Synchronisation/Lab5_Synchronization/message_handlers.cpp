#include "header_main.h"
#include "header_controls.h"

#define MAX_SIZE_BUF 50
#define MAX_SIZE_MESSAGE 1000

long WM_CREATE_ON(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    CreateWindow("static", "������� I-�� ��������� [A;B]", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 165, 10, 165, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("static", "����� ������� A:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 35, 140, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("static", "������ ������� B:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 60, 140, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("static", "����� ���������:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 85, 140, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,35,80,15,hwnd, (HMENU)ID_TEXTBOX_A,NULL,NULL);
    CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,60,80,15,hwnd, (HMENU)ID_TEXTBOX_B,NULL,NULL);
    CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,85,80,15,hwnd, (HMENU)ID_TEXTBOX_PARTS_1,NULL,NULL);

    CreateWindow("static", "������� II-�� ��������� [C;D]", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 165, 110, 165, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("static", "����� ������� C:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 135, 140, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("static", "������ ������� D:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 165, 140, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("static", "����� ���������:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 190, 140, 15, hwnd, NULL,NULL, NULL);
    CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,135,80,15,hwnd, (HMENU)ID_TEXTBOX_C,NULL,NULL);
    CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,165,80,15,hwnd, (HMENU)ID_TEXTBOX_D,NULL,NULL);
    CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,190,80,15,hwnd, (HMENU)ID_TEXTBOX_PARTS_2,NULL,NULL);
    CreateWindow("button","��������� ��������!",WS_CHILD|BS_PUSHBUTTON|WS_VISIBLE,165,215,165,20,hwnd,(HMENU)1,NULL,NULL);
    return 0;
}

long WM_COMMAND_ON(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if((HIWORD(wParam) == 0) && (LOWORD(wParam) == 1))
    {
        bool a = WaitForSingleObject(bufAB->hEventMain, 1) == WAIT_TIMEOUT;
        bool b = WaitForSingleObject(bufCD->hEventMain, 1) == WAIT_TIMEOUT;
        if(a && b) // ���� ��� ������� ��������� � ������������ ��������� (�.�. ������ ��� ���������� ��������), �� ����� �������� ������ �� ����� � �������� �� �� ����������.
        {
            char buf[MAX_SIZE_BUF];
            GetDlgItemText(hwnd,ID_TEXTBOX_A,buf,MAX_SIZE_BUF);
            bufAB->xBegin = atof(buf);

            GetDlgItemText(hwnd,ID_TEXTBOX_B,buf,MAX_SIZE_BUF);
            bufAB->xEnd = atof(buf);

            GetDlgItemText(hwnd,ID_TEXTBOX_C,buf,MAX_SIZE_BUF);
            bufCD->xBegin = atof(buf);

            GetDlgItemText(hwnd,ID_TEXTBOX_D,buf,MAX_SIZE_BUF);
            bufCD->xEnd = atof(buf);

            GetDlgItemText(hwnd,ID_TEXTBOX_PARTS_1,buf,MAX_SIZE_BUF);
            bufAB->numOfParts = atoi(buf);

            GetDlgItemText(hwnd,ID_TEXTBOX_PARTS_2,buf,MAX_SIZE_BUF);
            bufCD->numOfParts = atoi(buf);

            bufAB->deltaX = (bufAB->xEnd - bufAB->xBegin) / bufAB->numOfParts;
            bufCD->deltaX = (bufCD->xEnd - bufCD->xBegin) / bufCD->numOfParts;

            // ��������� ������� � ���������� ���������, ����� �������� ����������.
            SetEvent(bufAB->hEventMain);
            SetEvent(bufCD->hEventMain);
        }
        else // ���� ���� �� ���� ������� � ���������� ��������� (������), ������ ������ ��� ���������� ��� ������.
        {
            MessageBox(*hwndMain, "� ������ ������ �������� ��� �����������.\n�������� ��� ��������� � ��������� ����������.","���������", MB_OK|MB_ICONWARNING);
        }
    }
    return 0;
}

long WM_DESTROY_ON(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    SetEvent(hEventExit);
    SetEvent(bufAB->hEventMain);
    SetEvent(bufCD->hEventMain);
    SetEvent(bufAB->hEventReady);
    SetEvent(bufCD->hEventReady);
}
