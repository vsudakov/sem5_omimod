#ifndef HEADER_MAIN_H
#define HEADER_MAIN_H

#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <ctime>
#include <iostream>
#include <string.h>

#define MAX_SIZE_MESSAGE 1000
#define MAX_BUF_COEF_SIZE 1001

#define NUM_EVENTS 7
#define ID_EVENT_ALPHA 0
#define ID_EVENT_BETA 1
#define ID_EVENT_Y 2
#define ID_EVENT_GAMMA 3
#define ID_EVENT_INTEGRAND 4
#define ID_EVENT_FILE 5
#define ID_EVENT_INTEGRAL 6

#define NUM_THREADS 6
#define ID_THREAD_ALPHA 0
#define ID_THREAD_BETA 1
#define ID_THREAD_Y 2
#define ID_THREAD_GAMMA 3
#define ID_THREAD_INTEGRAND 4
#define ID_THREAD_FILE 5

// ������, ������������ � ���� ��������� ��� ������� ��������� (������) ����.
typedef struct
{
    char message[MAX_SIZE_MESSAGE];
    char messageLog[MAX_SIZE_MESSAGE];
    char intervalName[10];
    double xBegin, xEnd, xCurrent, deltaX;
    long numOfParts;
    double alpha[MAX_BUF_COEF_SIZE], beta[MAX_BUF_COEF_SIZE], gamma[MAX_BUF_COEF_SIZE], yValue[MAX_BUF_COEF_SIZE], func, integral;
    double result, time;

    HANDLE hEventMain;
    HANDLE hEventReady;
    HANDLE hEvents[NUM_EVENTS];
    HANDLE hThreads[NUM_THREADS];
} Buffer;

// ���������� ������� ��� ���� ����������:
extern Buffer *bufAB;
extern Buffer *bufCD;

// ����� ������ ��� ���� �������:
extern HANDLE hEventExit;
extern HANDLE hSemaphoreFile;

extern HWND *hwndMain;  // ��������� �� ������� ����

// lib_threads.cpp
DWORD WINAPI ThreadCalcIntegral(LPVOID lParam);
DWORD WINAPI ThreadCoefAlpha(LPVOID lParam);
DWORD WINAPI ThreadCoefBeta(LPVOID lParam);
DWORD WINAPI ThreadYValue(LPVOID lParam);
DWORD WINAPI ThreadCoefGamma(LPVOID lParam);
DWORD WINAPI ThreadIntegrand(LPVOID lParam);
DWORD WINAPI ThreadOutputToFile(LPVOID lParam);
DWORD WINAPI ThreadOutputCommonInfo(LPVOID lParam);

//message_handlers.cpp
long WM_CREATE_ON(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
long WM_COMMAND_ON(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
long WM_DESTROY_ON(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);


#endif // LIB_MAIN_H
