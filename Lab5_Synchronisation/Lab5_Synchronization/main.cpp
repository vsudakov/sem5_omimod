#include "header_main.h"

// ����������� ���������� ����������:
Buffer *bufAB;
Buffer *bufCD;

HANDLE hEventExit;
HANDLE hSemaphoreFile;

HWND *hwndMain;
// end ����������� ���������� ����������

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
char szClassName[ ] = "CodeBlocksWindowsApp";

int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    setlocale(LC_ALL, "Russian");
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_WINDOW;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           "������������ ������ �5 - ������������� ������",       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           500,                 /* The programs width */
           285,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);

    hwndMain = &hwnd;

    // ��������� ������ ��� ������ ��� ���� ����������:
    bufAB = new Buffer;
    strcpy(bufAB->intervalName, "AB");
    bufCD = new Buffer;
    strcpy(bufCD->intervalName, "CD");

    // �������� ������ ������� � ������������ ���������:
    bufAB->hEventMain = CreateEvent(NULL,TRUE, FALSE, NULL);
    bufCD->hEventMain = CreateEvent(NULL,TRUE, FALSE, NULL);
    bufAB->hEventReady = CreateEvent(NULL,TRUE, FALSE, NULL);
    bufCD->hEventReady = CreateEvent(NULL,TRUE, FALSE, NULL);
    hEventExit = CreateEvent(NULL,TRUE, FALSE, NULL);
    hSemaphoreFile = CreateSemaphore(NULL, 1, 1, NULL);

    // �������� ������� ��� ���������� ���������� �� �������� AB � CD:
    HANDLE hThreadCalcAB = CreateThread(NULL,0,ThreadCalcIntegral,bufAB,0,NULL);
    HANDLE hThreadCalcCD = CreateThread(NULL,0,ThreadCalcIntegral,bufCD,0,NULL);
    HANDLE hThreadFollow = CreateThread(NULL,0,ThreadOutputCommonInfo,NULL,0,NULL);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    // �������� ���������� ������� ����������:
    WaitForSingleObject(hThreadCalcAB, INFINITE);
    WaitForSingleObject(hThreadCalcCD, INFINITE);
    WaitForSingleObject(hThreadFollow, INFINITE);

    // ������������ ��������:
    CloseHandle(hThreadCalcAB);
    CloseHandle(hThreadCalcCD);
    CloseHandle(hThreadFollow);

    CloseHandle(hEventExit);
    CloseHandle(hSemaphoreFile);

    CloseHandle(bufAB->hEventMain);
    CloseHandle(bufCD->hEventMain);
    CloseHandle(bufAB->hEventReady);
    CloseHandle(bufCD->hEventReady);

    delete bufAB;
    delete bufCD;

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
        case WM_CREATE: // ��������� � �������� ����
            WM_CREATE_ON(hwnd, message, wParam, lParam);
            break;
        case WM_COMMAND: // ��������� � ������� ������
            WM_COMMAND_ON(hwnd, message, wParam, lParam);
            break;
        case WM_DESTROY: // ��������� � ���������� ���������
            WM_DESTROY_ON(hwnd, message, wParam, lParam);
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}
