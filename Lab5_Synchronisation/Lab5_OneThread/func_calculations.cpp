#include "lib_main.h"

// ��������� �������� y i-��.
double yValue(double x, int i)
{
    double res = 0;
    for (int j = 1; j < i + 1; j++)
        res += exp(1.0 / CoefAlfa(x, j)) + exp(1.0 / CoefBeta(x, j));
    return res;
}

// ��������� ����������� �����.
double CoefAlfa(double x, int i)
{
    return sin(x) * i;
}

// ��������� ����������� ����.
double CoefBeta(double x, int i)
{
    return cos(x) * i * CoefAlfa(x, i);
}

// ��������� ����������� �����.
double CoefGamma(double x, int i)
{
    return sin(x) * cos(yValue(x, i)) * i;
}

// ��������������� �������.
double Integrand(double x)
{
    double res = 0;
    for (int i = 0; i < 1000 + 1; i++)
    {
        res += (CoefAlfa(x, i) + CoefBeta(x, i) + CoefGamma(x, i));
    }
    res = res * log(1.0 + x);
    return res;
}

// ������� �������� ��������� ������� ����� ���������������. (��������� �������� - ����� ���������)
double CalcIntegralByMethodOfLeftRectangles(double (* pIntegrand) (double x), double xBegin, double xEnd, long numberOfParts)
{
    double res = 0;
    double x;
    double delta = (xEnd - xBegin) / numberOfParts;

    for (int i = 0; i < numberOfParts; i++)
    {
        x = xBegin + i * delta;
        res += pIntegrand(x) * delta;
    }

    return res;
}
