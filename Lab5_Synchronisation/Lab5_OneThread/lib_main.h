#ifndef LIB_MAIN_H
#define LIB_MAIN_H

#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <string.h>
#include <ctime>
#include <math.h>

#define MAX_SIZE_BUF 50
#define MAX_SIZE_MESSAGE 1000

#define ID_TEXTBOX_A 2
#define ID_TEXTBOX_B 3
#define ID_TEXTBOX_C 4
#define ID_TEXTBOX_D 5
#define ID_TEXTBOX_PARTS_1 6
#define ID_TEXTBOX_PARTS_2 7

// ��������� ��� ������ ������
struct CalcData
{
    double A, B, C, D;
    long numOfParts1, numOfParts2;
};

// ���������� ���������� ����������:
extern HWND *hwndMain;  // ��������� �� ������� ����
extern HANDLE *pEvent;   // ��������� �� ������� ��� ����������
extern CalcData *buffer; // ����� ��� ������������ ������

// func_calculations.cpp
double yValue(double x, int i);
double CoefAlfa(double x, int i);
double CoefBeta(double x, int i);
double CoefGamma(double x, int i);
double Integrand(double x);
double CalcIntegralByMethodOfLeftRectangles(double (* pIntegrand) (double x), double xBegin, double xEnd, long numberOfParts);

// func_help.cpp
bool LogOut(char *text);

// func_threads.cpp
DWORD WINAPI ThreadProc(LPVOID lParam);

#endif // LIB_MAIN_H
