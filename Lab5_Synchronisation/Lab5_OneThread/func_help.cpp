#include "lib_main.h"

bool LogOut(char *text)
{
    FILE* file = fopen("log.txt","a");
    if(file == NULL) return false;

    time_t seconds = time(NULL);
    tm* timeinfo = localtime(&seconds);

    fprintf(file, "\n\n");
    fprintf(file, asctime(timeinfo));
    fprintf(file, text);

    fclose(file);

    delete(timeinfo);
    return true;
}
