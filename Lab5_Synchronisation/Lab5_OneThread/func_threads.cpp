#include "lib_main.h"

DWORD WINAPI ThreadProc(LPVOID lParam)
{
    CalcData *data = (CalcData *)lParam;
    while(true)
    {
        WaitForSingleObject(*pEvent, INFINITE); // ����� ������ �� ����������

        // ��������� �������� ��� ������� �������:
        double time1 = clock();
        double res1 = CalcIntegralByMethodOfLeftRectangles(Integrand, data->A, data->B, data->numOfParts1);
        time1 = clock() - time1;
        time1 = time1 / CLOCKS_PER_SEC;

        // ��������� �������� ��� ������� �������:
        double time2 = clock();
        double res2 = CalcIntegralByMethodOfLeftRectangles(Integrand, data->C, data->D, data->numOfParts2);
        time2 = clock() - time2;
        time2 = time2 / CLOCKS_PER_SEC;

        double commonTime = time1 + time2;

        // ������������ ��������� � ������������ ��� MessageBox � ����:
        char message[MAX_SIZE_MESSAGE];
        char messageLog[MAX_SIZE_MESSAGE];
        sprintf(messageLog, "         I-� ��������\n����� �������:    %f\n������ �������:   %f\n����� ���������:  %d\n���������:        %f\n����� ����������: %f\n--------------------------\n        II-� ��������\n����� �������:    %f\n������ �������:   %f\n����� ���������:  %d\n���������:        %f\n����� ����������: %f\n--------------------------\n����� �����:      %f",
                data->A, data->B, data->numOfParts1, res1, time1, data->C, data->D, data->numOfParts2, res2, time2, commonTime);
        sprintf(message, "              I-� ��������\n����� �������:          %f\n������ �������:       %f\n����� ���������:    %d\n���������:                   %f\n����� ����������: %f\n--------------------------------\n              II-� ��������\n����� �������:          %f\n������ �������:       %f\n����� ���������:    %d\n���������:                   %f\n����� ����������: %f\n--------------------------------\n����� �����:            %f",
                data->A, data->B, data->numOfParts1, res1, time1, data->C, data->D, data->numOfParts2, res2, time2, commonTime);
        LogOut(messageLog);
        MessageBox(*hwndMain, message,"���������", MB_OK|MB_ICONINFORMATION);

        ResetEvent(*pEvent); // �������� �������
    }
    return 0;
}
