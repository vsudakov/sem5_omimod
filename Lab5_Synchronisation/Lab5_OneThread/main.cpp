#include "lib_main.h"

// ���������� ���������� ����������:
HWND *hwndMain;
HANDLE *pEvent = new HANDLE;
CalcData *buffer = new CalcData;

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
char szClassName[ ] = "Lab5 - Single-threaded application";


int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{

    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_WINDOW;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           "������������ ������ �5 - ������������ ������",       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           500,                 /* The programs width */
           285,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL ,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    hwndMain = &hwnd;
    *pEvent = CreateEvent(NULL,TRUE, FALSE, NULL); // �������� ������� � ������������ ���������
    HANDLE hCalc = CreateThread(NULL,0,ThreadProc,buffer,0,NULL); // �������� ������ ��� ������� ���������� ���������

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    CloseHandle(hCalc);
    CloseHandle(pEvent);

    delete hwndMain, pEvent, buffer;

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}

/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
    case WM_CREATE: // ��������� � �������� ����
        {
        CreateWindow("static", "������� I-�� ��������� [A;B]", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 165, 10, 165, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("static", "����� ������� A:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 35, 140, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("static", "������ ������� B:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 60, 140, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("static", "����� ���������:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 85, 140, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,35,80,15,hwnd, (HMENU)ID_TEXTBOX_A,NULL,NULL);
        CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,60,80,15,hwnd, (HMENU)ID_TEXTBOX_B,NULL,NULL);
        CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,85,80,15,hwnd, (HMENU)ID_TEXTBOX_PARTS_1,NULL,NULL);

        CreateWindow("static", "������� II-�� ��������� [C;D]", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 165, 110, 165, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("static", "����� ������� C:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 135, 140, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("static", "������ ������� D:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 165, 140, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("static", "����� ���������:", WS_CHILD | WS_VISIBLE | WS_TABSTOP, 10, 190, 140, 15, hwnd, NULL,NULL, NULL);
        CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,135,80,15,hwnd, (HMENU)ID_TEXTBOX_C,NULL,NULL);
        CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,165,80,15,hwnd, (HMENU)ID_TEXTBOX_D,NULL,NULL);
        CreateWindow("edit", NULL, WS_BORDER | WS_CHILD | WS_VISIBLE | NULL | NULL ,140,190,80,15,hwnd, (HMENU)ID_TEXTBOX_PARTS_2,NULL,NULL);
        CreateWindow("button","��������� ��������!",WS_CHILD|BS_PUSHBUTTON|WS_VISIBLE,165,215,165,20,hwnd,(HMENU)1,NULL,NULL);
        break;
        }
    case WM_COMMAND: // ��������� � ������� ������
        if((HIWORD(wParam) == 0) && (LOWORD(wParam) == 1))
        {
            if(WaitForSingleObject(*pEvent, 1) == WAIT_TIMEOUT) // ���� ������� ��������, �� ������� �� ����������
            {
                char buf[MAX_SIZE_BUF];
                GetDlgItemText(hwnd,ID_TEXTBOX_A,buf,MAX_SIZE_BUF);
                buffer->A = atof(buf);
                GetDlgItemText(hwnd,ID_TEXTBOX_B,buf,MAX_SIZE_BUF);
                buffer->B = atof(buf);
                GetDlgItemText(hwnd,ID_TEXTBOX_C,buf,MAX_SIZE_BUF);
                buffer->C = atof(buf);
                GetDlgItemText(hwnd,ID_TEXTBOX_D,buf,MAX_SIZE_BUF);
                buffer->D = atof(buf);
                GetDlgItemText(hwnd,ID_TEXTBOX_PARTS_1,buf,MAX_SIZE_BUF);
                buffer->numOfParts1 = atoi(buf);
                GetDlgItemText(hwnd,ID_TEXTBOX_PARTS_2,buf,MAX_SIZE_BUF);
                buffer->numOfParts2 = atoi(buf);
                SetEvent(*pEvent);
            }
            else // ���� ������� ������, ������ ��� �����������.
            {
                MessageBox(hwnd, "� ������ ������ �������� ��� �����������.\n�������� ��� ��������� � ��������� ����������.","���������", MB_OK|MB_ICONWARNING);
            }
        }
        break;
    case WM_DESTROY:
        PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
        break;
    default:                      /* for messages that we don't deal with */
        return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}



