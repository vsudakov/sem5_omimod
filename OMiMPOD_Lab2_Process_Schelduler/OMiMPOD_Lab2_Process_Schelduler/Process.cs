﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMiMPOD_Lab2_Process_Schelduler
{
    enum StatusProcess
    {
        Г, И
    }

    class Process
    {
        public string Name;
        public StatusProcess Status;
        public int Length;
        public int Priority;
        public int RemainingTime;

        public Process(string name_, StatusProcess status_, int length_, int priority_, int remainingTime_)
        {
            Name = name_;
            Status = status_;
            Length = length_;
            Priority = priority_;
            RemainingTime = remainingTime_;
        }

        public static Process operator --(Process x)
        {
            x.RemainingTime--;
            if (x.RemainingTime < 0)
                throw new Exception();
            else
                return x;
        }

    }
}
