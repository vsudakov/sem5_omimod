﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMiMPOD_Lab2_Process_Schelduler
{
    class Scheduler
    {
        List<Process> processes;
        List<Process> deletedProcesses;

        Process currentExecuting;

        public Scheduler()
        {
            processes = new List<Process>();
            deletedProcesses = new List<Process>();
            currentExecuting = null;
        }

        public void ShowInfo()
        {
            if (processes.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Очередь процессов пуста.");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.WriteLine("╔═══╤═════╤═══════════╤══════════════╤═══════════╤══════════╗");
                Console.WriteLine("║ № │ Имя │ Состояние │ Длительность │ Приоритет │ Осталось ║");
                Console.WriteLine("╟───┼─────┼───────────┼──────────────┼───────────┼──────────╢");
                for (int i = 0; i < processes.Count; i++)
                {
                    Process temp = processes[i];
                    Console.WriteLine("║{0,3}│{1,5}│{2,11}│{3,14}│{4,11}│{5,10}║", i + 1, temp.Name, temp.Status, temp.Length, temp.Priority, temp.RemainingTime);
                }
                Console.WriteLine("╚═══╧═════╧═══════════╧══════════════╧═══════════╧══════════╝");
            }
            if (deletedProcesses.Count > 0)
            {
                Console.WriteLine("Завершённые процессы:");
                Console.WriteLine("╔═══╤═════╤══════════════╤═══════════╗");
                Console.WriteLine("║ № │ Имя │ Длительность │ Приоритет ║");
                Console.WriteLine("╟───┼─────┼──────────────┼───────────╢");
                for (int i = 0; i < deletedProcesses.Count; i++)
                {
                    Process temp = deletedProcesses[i];
                    Console.WriteLine("║{0,3}│{1,5}│{2,14}│{3,11}║", i + 1, temp.Name, temp.Length, temp.Priority);
                }
                Console.WriteLine("╚═══╧═════╧══════════════╧═══════════╝");
            }
        }

        public Process ChooseProcessForExecution()
        {
            Process res = null;

            if (processes.Count > 0)
            {
                res = processes[0];

                for(int i = 1; i < processes.Count; i++)
                    if (processes[i].Priority < res.Priority)
                        res = processes[i];

                for (int i = 0; i < processes.Count; i++)
                    if (processes[i].Priority == res.Priority)
                        if (processes[i].RemainingTime < res.RemainingTime)
                            res = processes[i];
            }

            return res;
        }

        public void AddProcess()
        {
            ShowInfo();

            if (processes.Count > 7)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Процесс не может быть добавлен, т.к. максимальное число процессов в очереди: 8.");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("\nДля продолжения нажмите любую клавишу . . .");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Создание нового процесса. Введите информацию:");
            Console.Write("имя процесса: ");
            string name = Console.ReadLine();
            
            int length = 0;
            while(true)
            {
                Console.Write("длительность: ");
                try
                {
                    length = Convert.ToInt32(Console.ReadLine());
                }
                catch { }

                if(length > 0) break;
                else
                {
                    Console.WriteLine("Некорректно введена длительность. Поробуйте ещё раз . . .");
                    Console.ReadKey();
                }
            }
             
            int priority = -1;
            while(true)
            {
                Console.Write("приоритет:    ");
                try
                {
                    priority = Convert.ToInt32(Console.ReadLine());
                }
                catch { }

                if(priority >= 0) break;
                else
                {
                    Console.WriteLine("Некорректно введён приоритет. Поробуйте ещё раз . . .");
                    Console.ReadKey();
                }
            }

            processes.Add(new Process(name, StatusProcess.Г, length, priority, length));

            Console.WriteLine("Процесс добавлен.\nДля продолжения нажмите любую клавишу . . .");
            Console.ReadKey();
        }

        public void RemoveProcess()

        {
            ShowInfo();

            int num = 0;
            while (true)
            {
                Console.Write("Номер удаляемого процесса: ");
                try
                {
                    num = Convert.ToInt32(Console.ReadLine());
                }
                catch { }

                if (num > 0 && num <= processes.Count) break;
                else
                {
                    Console.WriteLine("Некорректно введен номер. Поробуйте ещё раз . . .");
                    Console.ReadKey();
                }
           }

            try
            {
                num--;
                if (processes[num] == currentExecuting) currentExecuting = null;
                deletedProcesses.Add(processes[num]);
                processes.RemoveAt(num);
                Console.WriteLine("Процесс успешно удалён.");
            }
            catch
            {
                Console.WriteLine("Неизвестная ошибка.");
            }

            Console.WriteLine("\nДля продолжения нажмите любую клавишу . . .");
            Console.ReadKey();
        }

        public void NextClockCycle()
        {
            if (currentExecuting != null)
            {
                try { currentExecuting--; }
                catch 
                { 
                    processes.Remove(currentExecuting);
                    deletedProcesses.Add(currentExecuting);
                    currentExecuting = null; 
                }
            }

            if (currentExecuting == null)
            {
                currentExecuting = ChooseProcessForExecution();
                if(currentExecuting != null) currentExecuting.Status = StatusProcess.И;
            }
        }
    }
}
