﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OMiMPOD_Lab2_Process_Schelduler
{
    class Program
    {
        static void Main(string[] args)
        {
            int currentTime = 0;

            Scheduler scheduler = new Scheduler();

            bool exit = false;
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine("Планировщик процессов.");
                Console.WriteLine("Текущий квант времени: " + currentTime);
                scheduler.ShowInfo();
                Console.WriteLine("1. Добавить новый процесс в очередь.");
                Console.WriteLine("2. Принудительно завершить процесс.");
                Console.WriteLine("3. Следующий квант времени.");
                Console.WriteLine("0. Выход из программы.");
                Console.Write("Ваше действие: ");
                
                int menu = -1;
                try
                {
                    menu = Convert.ToInt32(Console.ReadLine());
                }
                catch { }

                switch (menu)
                {
                    case 1 :
                        Console.Clear();
                        scheduler.AddProcess();
                        break;
                    case 2 :
                        Console.Clear();
                        scheduler.RemoveProcess();
                        break;
                    case 3 :
                        currentTime++;
                        scheduler.NextClockCycle();
                        break;
                    case 0 :
                        exit = true;
                        Console.WriteLine("Для выхода из программы нажмите любую клавишу . . .");
                        Console.ReadKey();
                        break;
                    default :
                        Console.WriteLine("Не верно введён пункт меню. Для продолжения нажмите любую клавишу . . .");
                        Console.ReadKey();
                        break;
                }
            }
        }
    }
}
