#include "cyclic_queue.h"

CYCLIC_QUEUE* CreateCyclicQueue(int sizeOfCQ)
{
    CYCLIC_QUEUE *res = new CYCLIC_QUEUE;

    res->data = new double[sizeOfCQ];
    res->pBeginQueue = res->data;
    res->pEndQueue = res->data;
    res->pBeginMemory = res->data;
    res->pEndMemory = res->data + (sizeOfCQ - 1);
    res->isEmpty = true;

    return res;
}

void DeleteCyclicQueue(CYCLIC_QUEUE* pCQ)
{
    delete[] pCQ->data;
    delete pCQ->pBeginQueue, pCQ->pEndQueue, pCQ->pBeginMemory, pCQ->pEndMemory;
    delete pCQ;
}

double* GetNextAdressInMemory(double *p, CYCLIC_QUEUE *q)
{
    if(p == q->pEndMemory) p = q->pBeginMemory;
    else p++;
    return p;
}

bool Enqueue(double value, CYCLIC_QUEUE *q)
{
    double *nextAddress = GetNextAdressInMemory(q->pEndQueue, q);
    if(q->pBeginQueue == nextAddress) return false;

    if(q->isEmpty) q->isEmpty = false;
    else q->pEndQueue = nextAddress;

    *(q->pEndQueue) = value;

    return true;
}

bool Dequeue(double &value, CYCLIC_QUEUE *q)
{
    if(q->isEmpty) return false;

    value = *(q->pBeginQueue);

    if(q->pBeginQueue == q->pEndQueue) q->isEmpty = true;
    else q->pBeginQueue = GetNextAdressInMemory(q->pBeginQueue, q);

    return true;
}
