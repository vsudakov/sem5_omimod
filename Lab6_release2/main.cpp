#include "cyclic_queue.h"
#include "main.h"

CYCLIC_QUEUE *queue1, *queue2;
HANDLE hMutexFree1, hMutexFree2;
HANDLE hSemaphoreEmpty1, hSemaphoreEmpty2;
HANDLE hSemaphoreFull1, hSemaphoreFull2;

int main()
{
    setlocale(0, "RUS");

    bool exit = false;
    while(!exit)
    {
        int menu;
        // ����� ����:
        system("cls");
        puts("������������ ������ �6. ������������ ����-�����.\n");
        puts("�������� ��������:");
        puts("1. �������� ������������� 2 ����� � ��������� �������.");
        puts("2. ����������� ����� � ��������� �������.");
        puts("3. ��������� �� ����������.");
        puts("0. �����.");
        printf("\n��� �����: ");
        fflush(stdin); scanf("%d", &menu);

        // ��������� ������:
        switch(menu)
        {
        case 1:
            RandomGenerateSourceFilesHelp();
            break;
        case 2:
            LookSourceFilesHelp();
            break;
        case 3:
            Calculate();
            break;
        case 0:
            exit = true;
            printf("\n��� ������ ������� ����� ������� . . . ");
            fflush(stdin); getchar();
            break;
        default:
            printf("\n������. ����������� ����� ����� ����.\n");
            printf("����� ����������� ��� ��� ������� ����� ������� . . .");
            fflush(stdin); getchar();
            break;
        }
    }

    return 0;
}

DWORD WINAPI ThreadProducer1(LPVOID lParam)
{
    double value;
    int numOfVectors, numOfElements;

    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile("source1.dat", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING|FILE_ATTRIBUTE_READONLY, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);

    ov.Offset = 0;
    ReadFile(hFile, &numOfVectors,    sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);
    ReadFile(hFile, &numOfElements, sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);

    while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
    int *buffer = new int[numOfElements];

    for (int i = 0; i < numOfVectors; i++)
    {
        ReadFile(hFile, buffer, sizeof(int) * numOfElements, &dwCount, &ov);
        while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
        ov.Offset += sizeof(int) * numOfElements;

        value = 0;
        for (int j = 0; j < numOfElements; j++)
        {
            value += buffer[j] * buffer[j];
        }
        value = sqrt(value);

        WaitForSingleObject(hSemaphoreEmpty1, INFINITE);
        WaitForSingleObject(hMutexFree1, INFINITE);
        Enqueue(value, queue1);
        ReleaseMutex(hMutexFree1);
        ReleaseSemaphore(hSemaphoreFull1, 1, NULL);
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;

    return 0;
}

DWORD WINAPI ThreadProducer2(LPVOID lParam)
{
    double value;
    int numOfVectors, numOfElements;

    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile("source2.dat", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING|FILE_ATTRIBUTE_READONLY, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);

    ov.Offset = 0;
    ReadFile(hFile, &numOfVectors,    sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);
    ReadFile(hFile, &numOfElements, sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);

    while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
    int *buffer = new int[numOfElements];

    for (int i = 0; i < numOfVectors; i++)
    {
        ReadFile(hFile, buffer, sizeof(int) * numOfElements, &dwCount, &ov);
        while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
        ov.Offset += sizeof(int) * numOfElements;

        value = 0;
        for (int j = 0; j < numOfElements; j++)
        {
            value += buffer[j] * buffer[j];
        }
        value = sqrt(value);

        WaitForSingleObject(hSemaphoreEmpty2, INFINITE);
        WaitForSingleObject(hMutexFree2, INFINITE);
        Enqueue(value, queue2);
        ReleaseMutex(hMutexFree2);
        ReleaseSemaphore(hSemaphoreFull2, 1, NULL);
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;

    return 0;
}

DWORD WINAPI ThreadConsumer(LPVOID lParam)
{
    double lengthVector1;
    double lengthVector2;
    double mult;

    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile("result.txt", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    ov.Offset = 0;
    const int buffer_size = 50;
    char *buffer = new char[buffer_size];

    int index = 1;
    while(true)
    {
        // ������ ������ �� ������� ������
        if(WaitForSingleObject(hSemaphoreFull1, 1000) == WAIT_TIMEOUT) break;
        WaitForSingleObject(hMutexFree1, INFINITE);
        Dequeue(lengthVector1, queue1);
        ReleaseMutex(hMutexFree1);
        ReleaseSemaphore(hSemaphoreEmpty1, 1, NULL);

        // ������ ������ �� ������� ������
        if(WaitForSingleObject(hSemaphoreFull2, 1000) == WAIT_TIMEOUT) break;
        WaitForSingleObject(hMutexFree2, INFINITE);
        Dequeue(lengthVector2, queue2);
        ReleaseMutex(hMutexFree2);
        ReleaseSemaphore(hSemaphoreEmpty2, 1, NULL);

        mult = lengthVector1 * lengthVector2;
        sprintf(buffer, "%4d) %5f\n", index++, mult);
        WriteFile(hFile, buffer, strlen(buffer), &dwCount, &ov);
        ov.Offset += strlen(buffer);
        printf(buffer);
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;

    return 0;
}

bool RandomGenerateSourceFile(const char *nameFile, int numOfVectors, int numOfVectorValues)
{
    int *buffer = new int[numOfVectorValues];

    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile(nameFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    if(hFile == INVALID_HANDLE_VALUE) return false;

    ov.Offset = 0;
    WriteFile(hFile, &numOfVectors,      sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);
    WriteFile(hFile, &numOfVectorValues, sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);

    for (int i = 0; i < numOfVectors; i++)
    {
        for (int j = 0; j < numOfVectorValues; j++)
        {
            srand(clock());
            buffer[j] = rand() % 999;
            printf("%7d", buffer[j]);
            Sleep(1);
        }
        printf("\n");
        WriteFile(hFile, buffer, sizeof(int) * numOfVectorValues, &dwCount, &ov);
        while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
        ov.Offset += sizeof(int) * numOfVectorValues;
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;
    return true;
}

void RandomGenerateSourceFilesHelp()
{
    int numOfVectors, numOfVectorValues;
    system("cls");
    puts("��������� �������� ������ �� ���������� �������.\n");
    printf("������� ���������� �������� � ����� �����: ");
    fflush(stdin); scanf("%d", &numOfVectors);
    printf("������� ���������� �������� � ������ �������: ");
    fflush(stdin); scanf("%d",&numOfVectorValues);

    printf("\n��������� ������� �����:\n");
    if(!RandomGenerateSourceFile("source1.dat", numOfVectors, numOfVectorValues)) printf("������! ���� �� ������.\n");

    printf("\n��������� ������� �����:\n");
    if(!RandomGenerateSourceFile("source2.dat", numOfVectors, numOfVectorValues)) printf("������! ���� �� ������.\n");

    printf("\n�������� ����� \"sourse1.dat\" � \"sourse2.dat\" �� ���������� ������� �������������.\n��� �������� � ������� ���� ������� ����� ������� . . . ");
    fflush(stdin); getchar();
}

bool LookSourceFile(const char * nameFile)
{
    int numOfLines, numOfElements;

    OVERLAPPED ov;
    ov = {0};
    ov.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    DWORD dwCount;
    HANDLE hFile=CreateFile(nameFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING|FILE_ATTRIBUTE_READONLY, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    if(hFile == INVALID_HANDLE_VALUE) return false;


    ov.Offset = 0;
    ReadFile(hFile, &numOfLines,    sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);
    ReadFile(hFile, &numOfElements, sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);

    GetOverlappedResult(hFile, &ov, &dwCount, TRUE);
    int *buffer = new int[numOfElements];

    for (int i = 0; i < numOfLines; i++)
    {
        ReadFile(hFile, buffer, sizeof(int) * numOfElements, &dwCount, &ov);
        GetOverlappedResult(hFile, &ov, &dwCount, TRUE);
        ov.Offset += sizeof(int) * numOfElements;
        for (int j = 0; j < numOfElements; j++)
        {
            printf("%7d", buffer[j]);
        }
        printf("\n");
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;
    return true;
}

void LookSourceFilesHelp()
{
    system("cls");
    printf("�������� ������ � ��������� �������.");

    printf("\n\n�������� ���� \"source1.dat\" :\n");
    if(!LookSourceFile("source1.dat")) printf("������! ���� � ����� ������ �� ����������.\n");

    printf("\n�������� ���� \"source2.dat\" :\n");
    if(!LookSourceFile("source2.dat")) printf("������! ���� � ����� ������ �� ����������.\n");

    printf("\n��� �������� � ������� ���� ������� ����� ������� . . . ");
    fflush(stdin); getchar();
}

void Calculate()
{
    system("cls");
    printf("������ ���������� . . . ");

    queue1 = CreateCyclicQueue(BUFFER_SIZE);
    queue2 = CreateCyclicQueue(BUFFER_SIZE);

    hMutexFree1 = CreateMutex(NULL, FALSE, NULL); // �������� �������� � ��������� ���������
    hSemaphoreEmpty1 = CreateSemaphore(NULL, BUFFER_SIZE, BUFFER_SIZE, NULL);
    hSemaphoreFull1 = CreateSemaphore(NULL, 0, BUFFER_SIZE, NULL);

    hMutexFree2 = CreateMutex(NULL, FALSE, NULL); // �������� �������� � ��������� ���������
    hSemaphoreEmpty2 = CreateSemaphore(NULL, BUFFER_SIZE, BUFFER_SIZE, NULL);
    hSemaphoreFull2 = CreateSemaphore(NULL, 0, BUFFER_SIZE, NULL);

    HANDLE hThreadProducer1 = CreateThread(NULL, 0, ThreadProducer1, NULL, 0, NULL);
    HANDLE hThreadProducer2 = CreateThread(NULL, 0, ThreadProducer2, NULL, 0, NULL);
    HANDLE hThreadConsumer = CreateThread(NULL, 0, ThreadConsumer, NULL, 0, NULL);

    printf("\n��������� �������. ����������, ��������� . . . ");
    printf("\n\n������������ ��������:\n");

    WaitForSingleObject(hThreadProducer1, INFINITE);
    WaitForSingleObject(hThreadProducer2, INFINITE);
    WaitForSingleObject(hThreadConsumer, INFINITE);

    CloseHandle(hThreadProducer1);
    CloseHandle(hThreadProducer2);
    CloseHandle(hThreadConsumer);

    CloseHandle(hMutexFree1);
    CloseHandle(hSemaphoreEmpty1);
    CloseHandle(hSemaphoreFull1);

    CloseHandle(hMutexFree2);
    CloseHandle(hSemaphoreEmpty2);
    CloseHandle(hSemaphoreFull2);

    DeleteCyclicQueue(queue1);
    DeleteCyclicQueue(queue2);

    printf("\n���������� ���������. ��� �������� � ������� ���� ������� ����� ������� . . . ");
    fflush(stdin); getchar();
}
