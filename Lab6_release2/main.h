#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>

#define BUFFER_SIZE 100

DWORD WINAPI ThreadProducer1(LPVOID lParam);
DWORD WINAPI ThreadProducer2(LPVOID lParam);
DWORD WINAPI ThreadConsumer(LPVOID lParam);
bool RandomGenerateSourceFile(const char *nameFile, int numOfVectors, int numOfVectorValues);
void RandomGenerateSourceFilesHelp();
bool LookSourceFile(const char * nameFile);
void LookSourceFilesHelp();
void Calculate();

#endif // MAIN_H_INCLUDED
