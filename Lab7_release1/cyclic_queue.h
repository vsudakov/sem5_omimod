#ifndef CYCLIC_QUEUE_H_INCLUDED
#define CYCLIC_QUEUE_H_INCLUDED

typedef struct // ��������� ��� ����������� �������:
{
    double *data;
    double *pBeginQueue;
    double *pEndQueue;
    double *pBeginMemory;
    double *pEndMemory;
    bool isEmpty;
} CYCLIC_QUEUE;

CYCLIC_QUEUE* CreateCyclicQueue(int sizeOfCQ);
void DeleteCyclicQueue(CYCLIC_QUEUE* pCQ);
double* GetNextAdressInMemory(double *p, CYCLIC_QUEUE *q);
bool Enqueue(double value, CYCLIC_QUEUE *q);
bool Dequeue(double &value, CYCLIC_QUEUE *q);

#endif // CYCLIC_QUEUE_H_INCLUDED
