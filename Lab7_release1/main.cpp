#include "cyclic_queue.h"
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>

#define BUFFER_SIZE 100

int numOfVectorsGlob, numOfElementsGlob;
double *pToLengthes1, *pToLengthes2;

CYCLIC_QUEUE *queue1, *queue2, *queue3;
HANDLE hMutexFree1, hMutexFree2, hMutexFree3;
HANDLE hSemaphoreEmpty1, hSemaphoreEmpty2, hSemaphoreEmpty3;
HANDLE hSemaphoreFull1, hSemaphoreFull2, hSemaphoreFull3;

void ReadNumbersOfVectorsAndElements()
{
    DWORD dwCount;
    HANDLE hFile=CreateFile("source1.dat", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING|FILE_ATTRIBUTE_READONLY, FILE_ATTRIBUTE_NORMAL, NULL);
    ReadFile(hFile, &numOfVectorsGlob,   sizeof(int), &dwCount, NULL);
    ReadFile(hFile, &numOfElementsGlob,  sizeof(int), &dwCount, NULL);
    CloseHandle(hFile);
}

DWORD WINAPI ThreadProducer1(LPVOID lParam)
{
    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile("source1.dat", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING|FILE_ATTRIBUTE_READONLY, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    ov.Offset = sizeof(int) * 2;

    while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
    int *buffer = new int[numOfElementsGlob];

    for (int i = 0; i < numOfVectorsGlob; i++)
    {
        ReadFile(hFile, buffer, sizeof(int) * numOfElementsGlob, &dwCount, &ov);
        while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
        ov.Offset += sizeof(int) * numOfElementsGlob;

        for (int j = 0; j < numOfElementsGlob; j++)
        {
            WaitForSingleObject(hSemaphoreEmpty1, INFINITE);
            WaitForSingleObject(hMutexFree1, INFINITE);
            Enqueue(buffer[j], queue1);
            ReleaseMutex(hMutexFree1);
            ReleaseSemaphore(hSemaphoreFull1, 1, NULL);
        }
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;

    return 0;
}

DWORD WINAPI ThreadProducer2(LPVOID lParam)
{
    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile("source2.dat", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING|FILE_ATTRIBUTE_READONLY, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);

    ov.Offset = sizeof(int) * 2;

    while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
    int *buffer = new int[numOfElementsGlob];

    for (int i = 0; i < numOfVectorsGlob; i++)
    {
        ReadFile(hFile, buffer, sizeof(int) * numOfElementsGlob, &dwCount, &ov);
        while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
        ov.Offset += sizeof(int) * numOfElementsGlob;

        for (int j = 0; j < numOfElementsGlob; j++)
        {
            WaitForSingleObject(hSemaphoreEmpty2, INFINITE);
            WaitForSingleObject(hMutexFree2, INFINITE);
            Enqueue(buffer[j], queue2);
            ReleaseMutex(hMutexFree2);
            ReleaseSemaphore(hSemaphoreFull2, 1, NULL);
        }
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;

    return 0;
}

DWORD WINAPI ThreadConsumer(LPVOID lParam)
{
    double *vector1 = new double[numOfElementsGlob];
    double *vector2 = new double[numOfElementsGlob];
    double length1;
    double length2;

    *pToLengthes1 = numOfVectorsGlob;
    *pToLengthes2 = numOfElementsGlob;

    int index = 1;
    for (int i = 0; i < numOfVectorsGlob; i++)
    {
        for (int j = 0; j < numOfElementsGlob; j++)
        {
            WaitForSingleObject(hSemaphoreFull1, INFINITE);
            WaitForSingleObject(hMutexFree1, INFINITE);
            Dequeue(vector1[j], queue1);
            ReleaseMutex(hMutexFree1);
            ReleaseSemaphore(hSemaphoreEmpty1, 1, NULL);

            WaitForSingleObject(hSemaphoreFull2, INFINITE);
            WaitForSingleObject(hMutexFree2, INFINITE);
            Dequeue(vector2[j], queue2);
            ReleaseMutex(hMutexFree2);
            ReleaseSemaphore(hSemaphoreEmpty2, 1, NULL);
        }

        length1 = length2 = 0;
        for(int j = 0; j < numOfElementsGlob; j++)
        {
            length1 += vector1[j] * vector1[j];
            length2 += vector2[j] * vector2[j];
        }
        length1 = sqrt(length1);
        length2 = sqrt(length2);

        *(pToLengthes1 + i + 1) = length1;
        *(pToLengthes2 + i + 1) = length2;
    }

    delete[] vector1, vector2;

    return 0;
}

bool RandomGenerateSourceFile(const char *nameFile, int numOfVectors, int numOfVectorValues)
{
    int *buffer = new int[numOfVectorValues];

    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile(nameFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    if(hFile == INVALID_HANDLE_VALUE) return false;

    ov.Offset = 0;
    WriteFile(hFile, &numOfVectors,      sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);
    WriteFile(hFile, &numOfVectorValues, sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);

    for (int i = 0; i < numOfVectors; i++)
    {
        for (int j = 0; j < numOfVectorValues; j++)
        {
            srand(clock());
            buffer[j] = rand() % 999;
            printf("%7d", buffer[j]);
            Sleep(1);
        }
        printf("\n");
        WriteFile(hFile, buffer, sizeof(int) * numOfVectorValues, &dwCount, &ov);
        while(!GetOverlappedResult(hFile, &ov, &dwCount, FALSE));
        ov.Offset += sizeof(int) * numOfVectorValues;
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;
    return true;
}

void RandomGenerateSourceFilesHelp()
{
    int numOfVectors, numOfVectorValues;
    system("cls");
    puts("��������� �������� ������ �� ���������� �������.\n");
    printf("������� ���������� �������� � ����� �����: ");
    fflush(stdin); scanf("%d", &numOfVectors);
    printf("������� ���������� �������� � ������ �������: ");
    fflush(stdin); scanf("%d",&numOfVectorValues);

    printf("\n��������� ������� �����:\n");
    if(!RandomGenerateSourceFile("source1.dat", numOfVectors, numOfVectorValues)) printf("������! ���� �� ������.\n");

    printf("\n��������� ������� �����:\n");
    if(!RandomGenerateSourceFile("source2.dat", numOfVectors, numOfVectorValues)) printf("������! ���� �� ������.\n");

    printf("\n�������� ����� \"sourse1.dat\" � \"sourse2.dat\" �� ���������� ������� �������������.\n��� �������� � ������� ���� ������� ����� ������� . . . ");
    fflush(stdin); getchar();
}

bool LookSourceFile(const char * nameFile)
{
    int numOfLines, numOfElements;

    OVERLAPPED ov;
    ov = {0};
    ov.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    DWORD dwCount;
    HANDLE hFile=CreateFile(nameFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING|FILE_ATTRIBUTE_READONLY, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    if(hFile == INVALID_HANDLE_VALUE) return false;


    ov.Offset = 0;
    ReadFile(hFile, &numOfLines,    sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);
    ReadFile(hFile, &numOfElements, sizeof(int), &dwCount, &ov);
    ov.Offset += sizeof(int);

    GetOverlappedResult(hFile, &ov, &dwCount, TRUE);
    int *buffer = new int[numOfElements];

    for (int i = 0; i < numOfLines; i++)
    {
        ReadFile(hFile, buffer, sizeof(int) * numOfElements, &dwCount, &ov);
        GetOverlappedResult(hFile, &ov, &dwCount, TRUE);
        ov.Offset += sizeof(int) * numOfElements;
        for (int j = 0; j < numOfElements; j++)
        {
            printf("%7d", buffer[j]);
        }
        printf("\n");
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;
    return true;
}

void LookSourceFilesHelp()
{
    system("cls");
    printf("�������� ������ � ��������� �������.");

    printf("\n\n�������� ���� \"source1.dat\" :\n");
    if(!LookSourceFile("source1.dat")) printf("������! ���� � ����� ������ �� ����������.\n");

    printf("\n�������� ���� \"source2.dat\" :\n");
    if(!LookSourceFile("source2.dat")) printf("������! ���� � ����� ������ �� ����������.\n");

    printf("\n��� �������� � ������� ���� ������� ����� ������� . . . ");
    fflush(stdin); getchar();
}

void Calculate()
{
    system("cls");
    printf("������ ���������� . . . ");

    // �������� �������� �����
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    memset (&si, 0, sizeof(si));
    memset (&pi, 0, sizeof(pi));
    si.cb = sizeof(si);

    HANDLE hMapping1 = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024, "Lab7_FileMap1");
    LPVOID pMapping1 = MapViewOfFile(hMapping1, FILE_MAP_WRITE, 0, 0, 0);
    pToLengthes1 = (double*)pMapping1;

    HANDLE hMapping2 = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024, "Lab7_FileMap2");
    LPVOID pMapping2 = MapViewOfFile(hMapping2, FILE_MAP_WRITE, 0, 0, 0);
    pToLengthes2 = (double*)pMapping2;
    // end ��������.

    // ���������� � ������ ����������� �������
    queue1 = CreateCyclicQueue(BUFFER_SIZE);
    queue2 = CreateCyclicQueue(BUFFER_SIZE);

    hMutexFree1 = CreateMutex(NULL, FALSE, NULL); // �������� �������� � ��������� ���������
    hSemaphoreEmpty1 = CreateSemaphore(NULL, BUFFER_SIZE, BUFFER_SIZE, NULL);
    hSemaphoreFull1 = CreateSemaphore(NULL, 0, BUFFER_SIZE, NULL);

    hMutexFree2 = CreateMutex(NULL, FALSE, NULL); // �������� �������� � ��������� ���������
    hSemaphoreEmpty2 = CreateSemaphore(NULL, BUFFER_SIZE, BUFFER_SIZE, NULL);
    hSemaphoreFull2 = CreateSemaphore(NULL, 0, BUFFER_SIZE, NULL);

    ReadNumbersOfVectorsAndElements();

    HANDLE hThreadProducer1 = CreateThread(NULL, 0, ThreadProducer1, NULL, 0, NULL);
    HANDLE hThreadProducer2 = CreateThread(NULL, 0, ThreadProducer2, NULL, 0, NULL);
    HANDLE hThreadConsumer = CreateThread(NULL, 0, ThreadConsumer, NULL, 0, NULL);

    printf("\n��������� �������. ����������, ��������� . . . ");
    printf("\n\n������������ ���� ��������:\n");

    WaitForSingleObject(hThreadProducer1, INFINITE);
    WaitForSingleObject(hThreadProducer2, INFINITE);
    WaitForSingleObject(hThreadConsumer, INFINITE);

    CloseHandle(hThreadProducer1);
    CloseHandle(hThreadProducer2);
    CloseHandle(hThreadConsumer);

    CloseHandle(hMutexFree1);
    CloseHandle(hSemaphoreEmpty1);
    CloseHandle(hSemaphoreFull1);

    CloseHandle(hMutexFree2);
    CloseHandle(hSemaphoreEmpty2);
    CloseHandle(hSemaphoreFull2);

    DeleteCyclicQueue(queue1);
    DeleteCyclicQueue(queue2);
    // end ������ ����������� �������.

    // ������ ����� ���������
    CreateProcess(
                  NULL,
                  "Lab7_release1.exe slave",
                  NULL,
                  NULL,
                  FALSE,
                  NORMAL_PRIORITY_CLASS,
                  NULL,
                  NULL,
                  &si,
                  &pi
                  );


    CloseHandle( pi.hThread );
    WaitForSingleObject( pi.hProcess, INFINITE );
    CloseHandle( pi.hProcess );

    UnmapViewOfFile(hMapping1);
    CloseHandle(hMapping1);
    UnmapViewOfFile(hMapping2);
    CloseHandle(hMapping2);

    printf("\n���������� ���������. ��� �������� � ������� ���� ������� ����� ������� . . . ");
    fflush(stdin); getchar();
}

void PrimaryCopy()
{
    bool exit = false;
    while(!exit)
    {
        int menu;
        // ����� ����:
        system("cls");
        puts("������������ ������ �6. ������������ ����-�����.\n");
        puts("�������� ��������:");
        puts("1. �������� ������������� 2 ����� � ��������� �������.");
        puts("2. ����������� ����� � ��������� �������.");
        puts("3. ��������� �� ����������.");
        puts("0. �����.");
        printf("\n��� �����: ");
        fflush(stdin); scanf("%d", &menu);

        // ��������� ������:
        switch(menu)
        {
        case 1:
            RandomGenerateSourceFilesHelp();
            break;
        case 2:
            LookSourceFilesHelp();
            break;
        case 3:
            Calculate();
            break;
        case 0:
            exit = true;
            printf("\n��� ������ ������� ����� ������� . . . ");
            fflush(stdin); getchar();
            break;
        default:
            printf("\n������. ����������� ����� ����� ����.\n");
            printf("����� ����������� ��� ��� ������� ����� ������� . . .");
            fflush(stdin); getchar();
            break;
        }
    }
}

DWORD WINAPI ThreadProducer3(LPVOID lParam)
{
    HANDLE hMapping1 = OpenFileMapping(FILE_MAP_READ, FALSE, "Lab7_FileMap1");
    LPVOID pMapping1 = MapViewOfFile(hMapping1, FILE_MAP_READ, 0, 0, 0);

    HANDLE hMapping2 = OpenFileMapping(FILE_MAP_READ, FALSE, "Lab7_FileMap2");
    LPVOID pMapping2 = MapViewOfFile(hMapping2, FILE_MAP_READ, 0, 0, 0);

    double *pLengthes1 = (double*) pMapping1;
    double *pLengthes2 = (double*) pMapping2;

    int numOfVectors = (int) *pLengthes1;

    double value;
    for(int i = 0; i < numOfVectors; i++)
    {
        value = *(pLengthes1 + 1 + i) * *(pLengthes2 + 1 + i);
        WaitForSingleObject(hSemaphoreEmpty3, INFINITE);
        WaitForSingleObject(hMutexFree3, INFINITE);
        Enqueue(value, queue3);
        ReleaseMutex(hMutexFree3);
        ReleaseSemaphore(hSemaphoreFull3, 1, NULL);
    }

    UnmapViewOfFile(hMapping1);
    CloseHandle(hMapping1);
    UnmapViewOfFile(hMapping2);
    CloseHandle(hMapping2);

    return 0;
}

DWORD WINAPI ThreadConsumer2(LPVOID lParam)
{
    double mult;

    OVERLAPPED ov;
    ov = {0};
    DWORD dwCount;
    HANDLE hFile=CreateFile("result.txt", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    ov.Offset = 0;

    int buffer_size = 20;
    char *buffer = new char[buffer_size];

    int index = 1;
    while (true)
    {
        if(WaitForSingleObject(hSemaphoreFull3, 1000) == WAIT_TIMEOUT) break;
        WaitForSingleObject(hMutexFree3, INFINITE);
        Dequeue(mult, queue3);
        ReleaseMutex(hMutexFree3);
        ReleaseSemaphore(hSemaphoreEmpty3, 1, NULL);

        sprintf(buffer, "%4d) %5f\n", index++, mult);
        WriteFile(hFile, buffer, strlen(buffer), &dwCount, &ov);
        ov.Offset += strlen(buffer);
        printf(buffer);
    }

    CloseHandle(hFile);
    ZeroMemory(&ov, sizeof(OVERLAPPED));
    delete[] buffer;

    return 0;
}

void SlaveCopy()
{
    queue3 = CreateCyclicQueue(BUFFER_SIZE);

    hMutexFree3 = CreateMutex(NULL, FALSE, NULL); // �������� �������� � ��������� ���������
    hSemaphoreEmpty3 = CreateSemaphore(NULL, BUFFER_SIZE, BUFFER_SIZE, NULL);
    hSemaphoreFull3 = CreateSemaphore(NULL, 0, BUFFER_SIZE, NULL);

    HANDLE hThreadProducer3 = CreateThread(NULL, 0, ThreadProducer3, NULL, 0, NULL);
    HANDLE hThreadConsumer2 = CreateThread(NULL, 0, ThreadConsumer2, NULL, 0, NULL);

    WaitForSingleObject(hThreadProducer3, INFINITE);
    WaitForSingleObject(hThreadConsumer2, INFINITE);

    CloseHandle(hThreadProducer3);
    CloseHandle(hThreadConsumer2);

    CloseHandle(hMutexFree3);
    CloseHandle(hSemaphoreFull3);
    CloseHandle(hSemaphoreEmpty3);

    DeleteCyclicQueue(queue3);
}

int main(int ac, char **av)
{
    setlocale(0, "RUS");

    if (ac != 2 || strcmp(av[1], "slave"))
    {
        PrimaryCopy();
    }
    else
    {
        SlaveCopy();
    }
    return 0;
}

